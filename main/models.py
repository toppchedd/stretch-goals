from django.db import models


class Where_From(models.Model):
    name = models.CharField(max_length=80)
    region = models.Charfield(max_length=100)
    bio = models.TextField()

    def __str__(self) -> str:
        return self.name
