from django.contrib import admin
from models import Where_From

@admin.register(Where_From)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name" ,
        "region",
        "bio",
    ]
